package com.sixteeneyes.daligoapi.service.message;

import com.sixteeneyes.daligoapi.entity.MessageSendHistory;
import com.sixteeneyes.daligoapi.enums.MessageTemplate;
import com.sixteeneyes.daligoapi.repository.MessageSendHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;

// 용도 : 카톡, 문자 등 메시지 발송해주는 팀.. 원래 알리고 api에서 여기서 호출하고 발송 내역 저장도 함
@Service
@RequiredArgsConstructor
public class MessageService {
    private final MessageSendHistoryRepository messageSendHistoryRepository;

    // 어떤 내용의 메시지를 누가 누구한테 어떤 내용으로 보낼 것이다. , 템플릿 안에 치환될 값
    public void sendMessage(MessageTemplate messageTemplate, String sendPhoneNumber, String receivePhoneNumber, HashMap<String, String> contents) {
        String messageContentOrigin = messageTemplate.getContents();
        for (String key:contents.keySet()) {
            String keyResult = "#\\{" + key +"\\}";
            messageContentOrigin = messageContentOrigin.replaceAll(keyResult, contents.get(key));

        }
        // replace - 교체하다

        MessageSendHistory messageSendHistory = new MessageSendHistory.Builder(sendPhoneNumber, receivePhoneNumber, messageContentOrigin).build();
        messageSendHistoryRepository.save(messageSendHistory);
    }
}
