package com.sixteeneyes.daligoapi.model;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FindUsernameResponse {
    @ApiModelProperty(notes = "아이디", required = true)
    private String username;

    private FindUsernameResponse(Builder builder) {
        this.username = builder.username;
    }

    public static class Builder implements CommonModelBuilder<FindUsernameResponse> {
        private final String username;

        public Builder(String username) {
            this.username = username;
        }

        @Override
        public FindUsernameResponse build() {
            return new FindUsernameResponse(this);
        }
    }
}
