package com.sixteeneyes.daligoapi.model.member;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    private Long id;

    @ApiModelProperty(notes = "회원 그룹", required = true)
    private String memberGroup;

    @ApiModelProperty(notes = "아이디", required = true)
    private String username;

    @ApiModelProperty(notes = "이름", required = true)
    private String name;

    private MemberItem(Builder builder) {
        this.id = builder.id;
        this.memberGroup = builder.memberGroup;
        this.username = builder.username;
        this.name = builder.name;
    }

    public static class Builder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final String memberGroup;
        private final String username;
        private final String name;

        public Builder(Member member) {
            this.id = member.getId();
            this.memberGroup = member.getMemberGroup().getName();
            this.username = member.getUsername();
            this.name = member.getName();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
