package com.sixteeneyes.daligoapi.model.member;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberResponse {
    @ApiModelProperty(notes = "회원 그룹", required = true)
    private String memberGroup;

    @ApiModelProperty(notes = "아이디", required = true)
    private String username;

    @ApiModelProperty(notes = "이름", required = true)
    private String name;

    @ApiModelProperty(notes = "연락처", required = true)
    private String phoneNumber;

    @ApiModelProperty(notes = "면허증 번호", required = true)
    private String licenseNumber;

    @ApiModelProperty(notes = "포인트", required = true)
    private BigDecimal point;

    @ApiModelProperty(notes = "가입일", required = true)
    private String dateCreate;

    @ApiModelProperty(notes = "활성화 여부", required = true)
    private String isEnabled;

    private MemberResponse(Builder builder) {
        this.memberGroup = builder.memberGroup;
        this.username = builder.username;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.licenseNumber = builder.licenseNumber;
        this.point = builder.point;
        this.dateCreate = builder.dateCreate;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<MemberResponse> {
        private final String memberGroup;
        private final String username;
        private final String name;
        private final String phoneNumber;
        private final String licenseNumber;
        private final BigDecimal point;
        private final String dateCreate;
        private final String isEnabled;

        public Builder(Member member) {
            this.memberGroup = member.getMemberGroup().getName();
            this.username = member.getUsername();
            this.name = member.getName();
            this.phoneNumber = member.getPhoneNumber();
            this.licenseNumber = member.getLicenseNumber();
            this.point = member.getPoint();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(member.getDateCreate());
            this.isEnabled = member.getIsEnabled() ? "O" : "X";
        }

        @Override
        public MemberResponse build() {
            return new MemberResponse(this);
        }
    }
}
