package com.sixteeneyes.daligoapi.model.review;

import com.sixteeneyes.daligoapi.entity.Review;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReviewItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    private Long id;

    @ApiModelProperty(notes = "이름", required = true)
    private String name;

    @ApiModelProperty(notes = "제목", required = true)
    private String title;

    @ApiModelProperty(notes = "작성일", required = true)
    private String dateWrite;

    private ReviewItem(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.title = builder.title;
        this.dateWrite = builder.dateWrite;
    }

    public static class Builder implements CommonModelBuilder<ReviewItem> {
        private final Long id;
        private final String name;
        private final String title;
        private final String dateWrite;

        public Builder(Review review) {
            this.id = review.getId();
            this.name = review.getName().charAt(0) + "0".repeat(review.getName().length() - 1);
            this.title = review.getTitle();
            this.dateWrite = CommonFormat.convertLocalDateTimeToString(review.getDateWrite());
        }

        @Override
        public ReviewItem build() {
            return new ReviewItem(this);
        }
    }
}
