package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistoryItem {
    @ApiModelProperty(notes = "시작일", required = true)
    private String dateStart;

    @ApiModelProperty(notes = "종료일", required = true)
    private String dateEnd;


    private KickBoardHistoryItem(Builder builder) {
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
    }

    public static class Builder implements CommonModelBuilder<KickBoardHistoryItem> {
        private final String dateStart;
        private final String dateEnd;

        public Builder(KickBoardHistory kickBoardHistory) {
            this.dateStart = CommonFormat.convertLocalDateTimeToString(kickBoardHistory.getDateStart());
            this.dateEnd = CommonFormat.convertLocalDateTimeToString(kickBoardHistory.getDateEnd());
        }

        @Override
        public KickBoardHistoryItem build() {
            return new KickBoardHistoryItem(this);
        }
    }
}
