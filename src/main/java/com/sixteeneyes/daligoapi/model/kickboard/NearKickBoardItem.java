package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.enums.KickBoardStatus;
import com.sixteeneyes.daligoapi.enums.PriceBasis;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NearKickBoardItem {
    @ApiModelProperty(notes = "킥보드 상태", required = true)
    private String kickBoardStatusName;

    @ApiModelProperty(notes = "모델명", required = true)
    private String modelName;

    @ApiModelProperty(notes = "기준요금", required = true)
    private String priceBasisName;

    @ApiModelProperty(notes = "위도", required = true)
    private Double posX;

    @ApiModelProperty(notes = "경도", required = true)
    private Double posY;

    @ApiModelProperty(notes = "거리", required = true)
    private Double distanceM;

    private NearKickBoardItem(Builder builder) {
        this.kickBoardStatusName = builder.kickBoardStatusName;
        this.modelName = builder.modelName;
        this.priceBasisName = builder.priceBasisName;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.distanceM = builder.distanceM;
    }

    public static class Builder implements CommonModelBuilder<NearKickBoardItem> {
        private final String kickBoardStatusName;
        private final String modelName;
        private final String priceBasisName;
        private final Double posX;
        private final Double posY;
        private final Double distanceM;

        public Builder(KickBoardStatus kickBoardStatus, String modelName, PriceBasis priceBasis, double posX, double posY, double distanceM) {
            this.kickBoardStatusName = kickBoardStatus.getName();
            this.modelName = modelName;
            this.priceBasisName = priceBasis.getName();
            this.posX = posX;
            this.posY = posY;
            this.distanceM = distanceM;
        }

        @Override
        public NearKickBoardItem build() {
            return new NearKickBoardItem(this);
        }
    }
}
