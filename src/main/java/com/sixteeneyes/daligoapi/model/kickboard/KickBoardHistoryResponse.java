package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistoryResponse {
    @ApiModelProperty(notes = "회원", required = true)
    private String member;

    @ApiModelProperty(notes = "킥보드", required = true)
    private String kickBoard;

    @ApiModelProperty(notes = "시작 위도", required = true)
    private Double startPosX;

    @ApiModelProperty(notes = "시작 경도", required = true)
    private Double startPosY;

    @ApiModelProperty(notes = "시작일", required = true)
    private String dateStart;

    @ApiModelProperty(notes = "종료 위도", required = true)
    private Double endPosX;

    @ApiModelProperty(notes = "종료 경도", required = true)
    private Double endPosY;

    @ApiModelProperty(notes = "기준일", required = true)
    private String dateBase;

    @ApiModelProperty(notes = "종료일", required = true)
    private String dateEnd;

    @ApiModelProperty(notes = "이용금액", required = true)
    private BigDecimal resultPrice;

    @ApiModelProperty(notes = "완료여부", required = true)
    private Boolean isComplete;

    private KickBoardHistoryResponse(Builder builder) {
        this.member = builder.member;
        this.kickBoard = builder.kickBoard;
        this.startPosX = builder.startPosX;
        this.startPosY = builder.startPosY;
        this.dateStart = builder.dateStart;
        this.endPosX = builder.endPosX;
        this.endPosY = builder.endPosY;
        this.dateBase = builder.dateBase;
        this.dateEnd = builder.dateEnd;
        this.resultPrice = builder.resultPrice;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<KickBoardHistoryResponse> {
        private final String member;
        private final String kickBoard;
        private final Double startPosX;
        private final Double startPosY;
        private final String dateStart;
        private final Double endPosX;
        private final Double endPosY;
        private final String dateBase;
        private final String dateEnd;
        private final BigDecimal resultPrice;
        private final Boolean isComplete;

        public Builder(KickBoardHistory kickBoardHistory) {
            this.member = kickBoardHistory.getMember().getName();
            this.kickBoard = kickBoardHistory.getKickBoard().getUniqueNumber();
            this.startPosX = kickBoardHistory.getStartPosX();
            this.startPosY = kickBoardHistory.getStartPosY();
            this.dateStart = CommonFormat.convertLocalDateTimeToString(kickBoardHistory.getDateStart());
            this.endPosX = kickBoardHistory.getEndPosX();
            this.endPosY = kickBoardHistory.getEndPosY();
            this.dateBase = CommonFormat.convertLocalDateToString(kickBoardHistory.getDateBase());
            this.dateEnd = CommonFormat.convertLocalDateTimeToString(kickBoardHistory.getDateEnd());
            this.resultPrice = kickBoardHistory.getResultPrice();
            this.isComplete = kickBoardHistory.getIsComplete();
        }
        @Override
        public KickBoardHistoryResponse build() {
            return new KickBoardHistoryResponse(this);
        }
    }
}
