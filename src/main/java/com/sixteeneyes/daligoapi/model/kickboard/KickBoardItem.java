package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoard;
import com.sixteeneyes.daligoapi.enums.KickBoardStatus;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    private Long id;

    @ApiModelProperty(notes = "고유번호", required = true)
    private String uniqueNumber;

    @ApiModelProperty(notes = "모델명", required = true)
    private String modelName;

    @ApiModelProperty(notes = "킥보드 상태", required = true)
    private String kickBoardStatus;

    private KickBoardItem(Builder builder) {
        this.id = builder.id;
        this.uniqueNumber = builder.uniqueNumber;
        this.modelName = builder.modelName;
        this.kickBoardStatus = builder.kickBoardStatus;
    }

    public static class Builder implements CommonModelBuilder<KickBoardItem> {
        private final Long id;
        private final String uniqueNumber;
        private final String modelName;
        private final String kickBoardStatus;

        public Builder(KickBoard kickBoard) {
            this.id = kickBoard.getId();
            this.uniqueNumber = kickBoard.getUniqueNumber();
            this.modelName = kickBoard.getModelName();
            this.kickBoardStatus = kickBoard.getKickBoardStatus().getName();
        }

        @Override
        public KickBoardItem build() {
            return new KickBoardItem(this);
        }
    }
}
