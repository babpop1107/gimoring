package com.sixteeneyes.daligoapi.model.kickboard;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class KickBoardUseRequest {
    @ApiModelProperty(notes = "위도", required = true)
    @NotNull
    private Double posX;

    @ApiModelProperty(notes = "경도", required = true)
    @NotNull
    private Double posY;
}
