package com.sixteeneyes.daligoapi.model;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistoryItem {
    @ApiModelProperty(notes = "회원", required = true)
    private String member;

    @ApiModelProperty(notes = "킥보드", required = true)
    private String kickBoard;

    @ApiModelProperty(notes = "사용 시작 시간", required = true)
    private String dateStart;

    @ApiModelProperty(notes = "사용 종료 시간", required = true)
    private String dateEnd;

    @ApiModelProperty(notes = "차감 금액", required = true)
    private BigDecimal deductedAmount;

    private KickBoardHistoryItem(Builder builder) {
        this.member = builder.member;
        this.kickBoard = builder.kickBoard;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.deductedAmount = builder.deductedAmount;
    }

    public static class Builder implements CommonModelBuilder<KickBoardHistoryItem> {
        private final String member;
        private final String kickBoard;
        private final String dateStart;
        private final String dateEnd;
        private final BigDecimal deductedAmount;

        public Builder(KickBoardHistory kickBoardHistory) {
            this.member = kickBoardHistory.getMember().getName();
            this.kickBoard = kickBoardHistory.getKickBoard().getUniqueNumber();
            this.dateStart = CommonFormat.convertLocalDateTimeToString(kickBoardHistory.getDateStart());
            this.dateEnd = CommonFormat.convertLocalDateTimeToString(kickBoardHistory.getDateEnd());
            this.deductedAmount = kickBoardHistory.getResultPrice();
        }

        @Override
        public KickBoardHistoryItem build() {
            return new KickBoardHistoryItem(this);
        }
    }
}