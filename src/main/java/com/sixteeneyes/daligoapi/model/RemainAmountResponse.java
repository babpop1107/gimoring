package com.sixteeneyes.daligoapi.model;

import com.sixteeneyes.daligoapi.entity.RemainingAmount;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RemainAmountResponse {
    @ApiModelProperty(notes = "잔여금액", required = true)
    private BigDecimal remainPrice;

    private RemainAmountResponse(Builder builder) {
        this.remainPrice = builder.remainPrice;
    }

    public static class Builder implements CommonModelBuilder<RemainAmountResponse> {
        private final BigDecimal remainPrice;

        public Builder(RemainingAmount remainingAmount) {
            this.remainPrice = remainingAmount.getRemainPrice();
        }

        @Override
        public RemainAmountResponse build() {
            return new RemainAmountResponse(this);
        }
    }
}
