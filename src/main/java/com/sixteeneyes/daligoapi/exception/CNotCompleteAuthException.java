package com.sixteeneyes.daligoapi.exception;

public class CNotCompleteAuthException extends RuntimeException {
    public CNotCompleteAuthException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotCompleteAuthException(String msg) {
        super(msg);
    }

    public CNotCompleteAuthException() {
        super();
    }
}