package com.sixteeneyes.daligoapi.exception;

public class CNotValidIdException extends RuntimeException{
    public CNotValidIdException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotValidIdException(String msg) {
        super(msg);
    }

    public CNotValidIdException() {
        super();
    }
}
