package com.sixteeneyes.daligoapi.repository;

import com.sixteeneyes.daligoapi.entity.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepository extends JpaRepository<Review, Long> {
    Page<Review> findAllByIdGreaterThanEqualOrderById(long id, Pageable pageable);
}
