package com.sixteeneyes.daligoapi.repository;

import com.sixteeneyes.daligoapi.entity.AmountChargingHistory;
import com.sixteeneyes.daligoapi.entity.Member.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmountChargingHistoryRepository extends JpaRepository<AmountChargingHistory, Long> {
    Page<AmountChargingHistory> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
    Page<AmountChargingHistory> findAllByMemberAndAndIdGreaterThanEqualOrderByIdDesc(Member member, long id, Pageable pageable);
}
