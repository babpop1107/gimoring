package com.sixteeneyes.daligoapi.controller;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.model.RemainAmountResponse;
import com.sixteeneyes.daligoapi.model.common.SingleResult;
import com.sixteeneyes.daligoapi.service.RemainAmountService;
import com.sixteeneyes.daligoapi.service.common.ResponseService;
import com.sixteeneyes.daligoapi.service.member.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "잔여 금액 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/remain-amount")
public class RemainAmountController {
    private final RemainAmountService remainAmountService;
    private final ProfileService profileService;

    @ApiOperation(value = "잔여 금액 보기")
    @GetMapping("/remain-price")
    public SingleResult<RemainAmountResponse> getRemainPrice() {
        Member member = profileService.getMemberData();
        return ResponseService.getSingleResult(remainAmountService.getRemainPrice(member));
    }
}
